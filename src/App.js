import "bootstrap/dist/css/bootstrap.min.css";
import AxiosLibrary from "./components/AxiosLibrary";
import FetchApi from "./components/FetchApi";

function App() {
  return (
    <div className="container mt-5">
      <FetchApi />
      <hr></hr>
      <AxiosLibrary />
    </div>
  );
}

export default App;
